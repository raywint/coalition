<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public static function getJsonFileUrl(){
		return app_path().'/storage/product_data/submissions.json';
	}

	public function index() {
		return View::make('form');
	}

	public function saveFormData(){
		
		$inputs = Input::all();
		
		$rules = array(
            'product_name' => 'required',
			'quantity' => 'required|integer',
			'item_price' => 'required|numeric'
        );

        ///////////////////
        // inputs title. //
        ///////////////////

        $titles = array(
        	'product_name' => 'Product name',
			'quantity' => 'Quantity in stock',
			'item_price' => 'Price per item'
        );

        $validotor = Validator::make($inputs, $rules);
		$validotor->setAttributeNames($titles);

		if($validotor->fails()){
			$messages = $validotor->messages();
			$errorMess = '';

			foreach ($messages->all() as $value) {
				$errorMess .= $value.'<br />';
			}

			return json_encode(array('error' => 1, 'message' => $errorMess));
		}

		$sJsonFile = self::getJsonFileUrl();
		$productData = array();

		if(file_exists($sJsonFile)){
			$productData = json_decode(file_get_contents($sJsonFile), true);
		}

		$productData[] = array(
			'product_name' => strip_tags($inputs['product_name']),
			'quantity' => $inputs['quantity'],
			'item_price' => $inputs['item_price'],
			'date_submitted' => (string) Carbon::now()
		);

		file_put_contents($sJsonFile, json_encode($productData));

		return json_encode(array('error' => 0, 'message' => 'Product data added successfully'));
	}

	public function loadTableData(){
		
		$aJson = array('data' => array());
		$sJsonFile = self::getJsonFileUrl();

		if(file_exists($sJsonFile)){
			$products = json_decode(file_get_contents($sJsonFile), true);
			if(count($products)){
				foreach ($products as $product) {
					$aJson['data'][] = array(
						$product['product_name'],
						$product['quantity'],
						$product['item_price'],
						$product['date_submitted'],
						$product['quantity'] * $product['item_price'],
					);
				}
			}
		}

		return json_encode($aJson);
	}

}
