<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		
		{{ HTML::style('assets/css/styles.css') }}

	</head>
	<body>
		<div class="row page-content">
			<div id="output-error" class="alert alert-danger" role="alert" style="display: none"></div>

			<div id="output-success" class="alert alert-success" role="alert" style="display: none"></div>
			<div class="">
				<form id="product-form" action="{{ URL::to('save') }}" method="POST">
					<fieldset class="form-group">
						<label for="exampleInputEmail1">Product Name</label>
						<input type="text" class="form-control" id="product-name" placeholder="Product Name" name="product_name">
					</fieldset>
					<fieldset class="form-group">
						<label for="exampleInputEmail1">Quantity in stock</label>
						<input type="text" class="form-control" id="quantity" placeholder="Quantity in stock" name="quantity">
					</fieldset>
					<fieldset class="form-group">
						<label for="exampleInputEmail1">Price per item</label>
						<input type="text" class="form-control" id="intem-price" placeholder="Price per item" name="item_price">
					</fieldset>
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
			<div class=""></div>
			<div class="">
				<div class="products-table-cont">
					<h2>Product Data</h2>
					<table id="products-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Product Name</th>
								<th>Quantity in stock</th>
								<th>Price per item</th>
								<th>Date submitted</th>
								<th>Total value number</th>
							</tr>
						</thead>
						<tfoot>
				            <tr>
				                <th colspan="4" style="text-align:right">Total:</th>
				                <th></th>
				            </tr>
				        </tfoot>
					</table>
				</div>
			</div>
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    	<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		
		<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

		<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>

		{{ HTML::script('assets/js/jquery.form.js') }}

		{{ HTML::script('assets/js/global.js') }}
		
		<script type="text/javascript">
			
			var oTable;
			
			$(document).ready(function() {
			    
				var options = { 
			        target:        '#output1',
			        success:       showResponse,
			        beforeSubmit:  function(){
			        	$('.alert').hide();
			        },
			        dataType:  'json'
			    }; 
			 
			    // bind form using 'ajaxForm' 
			    $('#product-form').ajaxForm(options);

			    oTable = $('#products-table').DataTable( {
			        "ajax": "{{ URL::to('load-table-data') }}",
			        "footerCallback": function ( row, data, start, end, display ) {
			            var api = this.api(), data;
			 
			            // Remove the formatting to get integer data for summation
			            var intVal = function ( i ) {
			                return typeof i === 'string' ?
			                    i.replace(/[\$,]/g, '')*1 :
			                    typeof i === 'number' ?
			                        i : 0;
			            };
			 
			            // Total over all pages
			            total = api
			                .column( 4 )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Total over this page
			            pageTotal = api
			                .column( 4, { page: 'current'} )
			                .data()
			                .reduce( function (a, b) {
			                    return intVal(a) + intVal(b);
			                }, 0 );
			 
			            // Update footer
			            $( api.column( 4 ).footer() ).html(
			                '$'+pageTotal +' ( $'+ total +' total)'
			            );
			        }
			    });

			    function showResponse(response, statusText, xhr, $form){
			    if(response.error == 1){
			        $('#output-error').html(response.message).fadeIn();
			    }
			    else{
			        $('#output-success').html(response.message).fadeIn();
			        oTable.ajax.reload();

			    }
			}
			});

			

		</script>
	</body>
</html>